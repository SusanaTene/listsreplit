#Statement
#Given an integer n, create a two-dimensional array of size n×n according to the following rules and print it:

#On the main diagonal put 0.
#On the diagonals adjacent to the main put 1.
#On the next adjacent diagonals put 2, and so forth.

# Read an integer:
n = int(input())
# Print a value:
# print(a)

a = [[abs(i - j) for j in range(n)] for i in range(n)]
for row in a:
  print(' '.join([str(i) for i in row]))