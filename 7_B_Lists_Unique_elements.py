#Given a list of numbers, find and print the elements that appear in it only once. Such elements should be
# printed in the order in which they occur in the original list.

# Read a list of integers:
a = [int(s) for s in input().split()]
# Print a value:
# print(a)
unico = []
repetidos = []
for x in a:
	if x not in unico:
		unico.append(x)
	else:
	  repetidos.append(x)
for i in repetidos:
  if i in unico:
    unico.remove(i)
for i in unico:
  print (i, end=' ')
