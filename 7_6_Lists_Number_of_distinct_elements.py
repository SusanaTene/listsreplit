#Given a list of numbers with all elements sorted in ascending order, determine and print
# the number of distinct elements in it.

# Read a list of integers:
a = [int(s) for s in input().split()]
# Print a value:
# print(a)
num =1
for i in range(1, len(a)):
  if a[i-1] != a[i]:
    num += 1
print(num)