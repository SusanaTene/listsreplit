print("        UNIVERSIDAD NACIONAL DE LOJA  ")
print("Autora: Lilia Susana Tene")
print("Email: liliatene@unl.edu.ec")
print("    -+-+-+-+-+- LISTS -+-+-+-+-+-")

#Given a list of numbers, find and print all its elements with even indices (i.e. A[0], A[2], A[4], ...).

# Read a list of integers:
a = [int(s) for s in input().split()]
# Print a value:
#print(a)
indice=0
for i in a:
  if indice %2==0:
    print (i)
  indice+=1
