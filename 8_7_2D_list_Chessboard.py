#Statement
#Given two positive integers n and m, create a two-dimensional array of size n×m and populate it with
# the characters "."and "*" in a chequered pattern. The top left corner should have the character "." .

# Read a list of integers:
n, m = [int(s) for s in input().split()]
# Print a value:
# print(a)

a = [["."]*m for i in range(n)]
for j in range (n):
    for k in range (m):
        if k%2 != 0 and j%2 == 0:
          a[j][k] = '*'
        elif k%2 == 0 and j%2 != 0:
          a[j][k] = '*'
for line in a:
    print(*line)