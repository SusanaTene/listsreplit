
#Given a list of integers, find the first maximum element in it. Print its value and its index (counting with 0).

# Read a list of integers:
a = [int(s) for s in input().split()]
# Print a value:
# print(a)
max_val = max(a)
index_max = a.index(max_val)
print(max_val, index_max)
