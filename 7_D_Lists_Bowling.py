#Statement
# In bowling, the player starts with 10 pins in a row at the far end of a lane. The goal is to knock all
# the pins down. For this assignment, the number of pins and balls will vary. Given the number of pins N and
# then the number of balls K to be rolled, followed by K pairs of numbers (one for each ball rolled), determine
# which pins remain standing after all the balls have been rolled.

#The balls are numbered from 1 to N for this situation. The subsequent number pairs, one for each K represent
# the first and last (inclusive) positions of the pins that were knocked down with each roll. Print a sequence
# of N characters, where "I" represents a pin left standing and "." represents a pin knocked down.

# Read a list of integers:
a = input()
# Print a value:
# print(a)

pin_bal = list(map(int, a.split()))
p = pin_bal[0]
b = pin_bal[1]
pos = []
n = 1
while n <= b:
    a =input()
    myList = list(map(int, a.split()))
    pos.append(myList[0])
    pos.append(myList[1])
    n += 1
pins = list(range(1,p+1))
n = 0
for n in range(0,len(pos)-1,2):
    if pos[n+1] > pos[n]:
        dist = list(range(pos[n],pos[n+1]+1))
    elif pos[n+1] < pos[n]:
        dist = list(range(pos[n+1],pos[n]+1))
    else:
        dist =[pos[n]]
    for d in dist:
        if d in pins:
            pins[pins.index(d)] = '.'
for pin in pins:
    if pin == '.':
        continue
    else:
        pins[pins.index(pin)] = 'I'
print(str(pins).replace('[','').replace(']','').replace("'","").replace(",",""))


