#Statement
#Given two integers - the number of rows m and columns n of m×n 2d list - and subsequent m rows of n integers,
# followed by two non-negative integers i and j less than n, swap the columns i and j of 2d list and print the
# result.


m, n = [int(s) for s in input().split()]
a = [[int(j) for j in input().split()] for i in range(m)]
col, fil = [int(s) for s in input().split()]
for i in range(m):
  a[i][col], a[i][fil] = a[i][fil], a[i][col]
for linea in a:
  print(*linea)
