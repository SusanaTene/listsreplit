#Statement
#Given an odd positive integer n, produce a two-dimensional array of size n×n. Fill each element with the
# character "." . Then fill the middle row, the middle column and the diagonals with the character "*".
# You'll get an image of a snow flake. Print the snow flake in n×n rows and columns and separate the characters
# with a single space.

# Read an integer:
n = int(input())
# Print a value:
# print(a)

a = [["."] * n for i in range(n)]
for y in range(n):
    for x in range(n):
        if x == y:
            a[y][x] = '*'
        elif x == (n - 1) / 2:
            a[y][x] = '*'
        elif x + y == 6:
            a[y][x] = '*'
        elif y == (n - 1) / 2:
            a[y][x] = '*'
for line in a:
    print(*line)
