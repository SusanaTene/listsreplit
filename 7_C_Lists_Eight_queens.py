#Statement
# It is possible to place 8 queens on an 8×8 chessboard so that no two queens threaten each other. Thus,
# it requires that no two queens share the same row, column, or diagonal.
#Given a placement of 8 queens on the chessboard. If there is a pair of queens that violates this rule,
#print YES, otherwise print NO. The input consists of eight coordinate pairs, one pair per line, with each pair
# giving the position of a queen on a standard chessboard with rows and columns numbered from 1 to 8.

# Read a list of integers:
#a = [int(s) for s in input().split()]
# Print a value:
# print(a)
posicion=[]
posicionfila=[]
posicioncolumna=[]
x= int(input("Ingrese cuantas reinas desea ingresar"))
i=0
print("Ingrese valor de la fila Y luego valor columna")
while i < (x):
  i=i+1
  a = [int(s) for s in input().split()]
  posicion.append(a[0])
  posicion.append(a[1])
#print(posicion)
for j in range(0,len(posicion),2):
  if posicion[j] not in posicionfila:
     posicionfila.append(posicion[j])
  else:
     print ('No')
     break
for x in range (1,len(posicion),2):
  if posicion[x] not in posicioncolumna:
     posicioncolumna.append(posicion[x])
  else:
     print ('No')
     break
#print(posicionfila)
#print(posicioncolumna)
bandera=0
for ia in range(len(posicionfila)):
  p1 = posicionfila[ia]
  p2 = posicioncolumna[ia]
  while p1>0 and p2>0 and bandera==0:
    p1=p1-1
    p2=p2-1
    if p1 in posicionfila:
      pc = posicioncolumna[p1-1]
      if p2 ==pc:
        bandera=1
for ib in range(len(posicionfila)):
  p1=posicionfila[ib]
  p2=posicioncolumna[ib]
  while p1<8 and p2>0 and bandera==0:
    p1=p1+1
    p2=p2-1
    if p1 in posicionfila:
      pc=posicioncolumna[p1-1]
      if p2==pc:
        bandera=1
for da in range(len(posicionfila)):
  p1=posicionfila[da]
  p2=posicioncolumna[da]
  while p1>0 and p2<8 and bandera==0:
    p1=p1-1
    p2=p2+1
    if p1 in posicionfila:
      pc=posicioncolumna[p1-1]
      if p2==pc:
        bandera=1
for db in range(len(posicionfila)):
  p1=posicionfila[db]
  p2=posicioncolumna[db]
  while p1<8 and p2<8 and bandera==0:
    p1=p1+1
    p2=p2+1
    if p1 in posicionfila:
      pc=posicioncolumna[p1-1]
      if p2==pc:
        bandera=1
if bandera == 1:
  print("NO")
else:
  print("SI")