print("        UNIVERSIDAD NACIONAL DE LOJA  ")
print("Autora: Lilia Susana Tene")
print("Email: liliatene@unl.edu.ec")
print("    -+-+-+-+-+- LISTS -+-+-+-+-+-")

#Given a list of numbers, print all its even elements. Use a for-loop that iterates
#over the list itself and not over its indices. That is, don't use range()

# Read a list of integers:
a = [int(s) for s in input().split()]
# Print a value:
# print(a)
for i in a:
  if i %2==0:
    print(i)