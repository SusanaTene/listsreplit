#Statement
#Given an integer n, create a two-dimensional array of size n×n according to the following rules and print it:

#On the antidiagonal put 1.
#On the diagonals above it put 0.
#On the diagonals below it put 2.

# Read an integer:
# a = int(input())
# Print a value:
# print(a)
datos = int(input())
a = [[0] * datos for i in range(datos)]
for i in range(datos):
  for j in range(datos):
    if i + j + 1 < datos:
      a[i][j] = 0
    elif i + j + 1 == datos:
      a[i][j] = 1
    else:
      a[i][j] = 2
for linea in a:
  print(linea)