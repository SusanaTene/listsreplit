
#Given a list of numbers, swap adjacent elements in each pair (swap A[0] with A[1], A[2] with A[3], etc.).
# Print the resulting list. If a list has an odd number of elements, leave the last element intact.

# Read a list of integers:
a = [int(s) for s in input().split()]
# Print a value:
# print (a)
salida = []
cont=0
while cont < (len(a)):
  if cont<len(a)-1:
    salida.append(a[cont+1])
  salida.append(a[cont])
  cont=cont+2
print(salida)