#Statement
#Given two integers - the number of rows m and columns n of m×n 2d list - and subsequent m rows of n integers,
#find the maximal element and print its row number and column number. If there are many maximal elements in
#different rows, report the one with smaller row number. If there are many maximal elements in the same row,
#report the one with smaller column number.

# Read a 2D list of integers:
# n = [[int(j) for j in input().split()] for i in range(NUM_ROWS)]
# Print a value:
# print(a)
m,n = [int(s) for s in input().split()]
for i in range(0,m):
    myList = list(map(int, input().split()))
    if i == 0:
        max_el = max(myList)
        maxList = [0,myList.index(max(myList))]
    elif max(myList) > max_el:
        max_el = max(myList)
        maxList = [i,myList.index(max(myList))]
print(*maxList, sep =' ')
