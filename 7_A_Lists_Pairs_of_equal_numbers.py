#Given a list of numbers, count a number of pairs of equal elements.
#Any two elements that are equal to each other should be counted exactly once.

# Read a list of integers:
a = [int(s) for s in input().split()]
# Print a value:
# print(a)
repetidos=[]
unicos=[]
cont=0
for n in a:
    if n not in unicos:
        unicos.append(n)
    else:
        repetidos.append(n)
for i in range(len(repetidos)):
  b= repetidos[i]
  for j in a:
    if b==j:
      cont =cont+1

print(int(cont/2) )